import { Injectable } from "@angular/core";

import { Query } from "./query";
import { Model } from "./model";

import { DebugService } from "../services/debug.service";

export class Settings extends Model {
    constructor() {
        super();
        let debug = new DebugService;
        super.init("settings", debug, new Query(debug));
    }
}
