import { Injectable } from "@angular/core";

import { Query } from "./query";
import { Model } from "./model";

import { DebugService } from "../services/debug.service";

export class Todo extends Model {
    constructor() {
        super();
        let debug = new DebugService;
        super.init("todo", debug, new Query(debug));
    }
}
