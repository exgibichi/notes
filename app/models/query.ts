import { Injectable } from "@angular/core";

import { checkSpecialWords } from "../libs/special-words";
import { DebugService } from "../services/debug.service";

declare let alasql: any;

@Injectable()
export class Query { // TODO: refactor split to builder and query
    private conn;
    private query;
    private params = [];
    private string_query: string = "";

    private show_databases_query: string = "SHOW DATABASES";
    private show_tables_query = "SHOW TABLES";

    private use_query: string = "USE {db}";
    private attach_query: string = "ATTACH {storage} DATABASE {db}";
    private create_db_query: string = "CREATE {storage} DATABASE IF NOT EXISTS {db}";

    private create_table_query = "CREATE TABLE {table}";
    private truncate_table_query = "TRUNCATE TABLE {table}";

    private select_query: string = "SELECT {select} ";
    private update_query: string = "UPDATE {table} SET {set} ";
    private delete_query: string = "DELETE FROM {table} ";
    private from_query: string = "FROM {from} ";
    private into_query: string = "INTO {table} ";
    private where_query: string = "WHERE {where} ";
    private order_query: string = "ORDER BY {order} ";
    private limit_query: string = "LIMIT {limit} ";

    constructor(private debug: DebugService) {
        this.query = {};
        this.conn = {};
    }

    setTable(table: string): void {
        this.conn.table = table;
    }

    setConnection(db: string, storage: string): void {
        this.conn.db = db;
        this.conn.storage = storage;
    }

    pureSQL(string_query: string): Query {
        this.string_query = string_query;
        return this;
    }

    select(select: any = '*'): Query {
        this.query.select = select;
        return this;
    }

    update(table?): Query {
        let table_for_update = this.conn.table;
        if (table) {
            table_for_update = table;
        }
        this.query.update = table_for_update;
        return this;
    }

    set(params): Query {
        this.query.set = params;
        return this;
    }

    from(from: any): Query {
        this.query.from = from;
        return this;
    }

    delete(delete_table: any): Query {
        this.query.delete = delete_table;
        return this;
    }

    into(into: any): Query {
        this.query.into = into;
        return this;
    }

    where(where: any): Query {
        this.query.where = where;
        return this;
    }

    order(order: any): Query {
        this.query.order = order;
        return this;
    }

    limit(limit: any): Query {
        this.query.limit = limit;
        return this;
    }

    createDB(storage?: string, db?: string): Query {
        this.string_query = this.create_db_query
        .replace("{storage}", this.conn.storage)
        .replace("{db}", this.conn.db);
        return this;
    }

    use(db?: string): Query {
        this.string_query = this.use_query.replace("{db}", this.conn.db);
        return this;
    }

    attach(storage?: string, db?: string): Query {
        this.string_query = this.attach_query
        .replace("{storage}", this.conn.storage)
        .replace("{db}", this.conn.db);
        return this;
    }

    createTable(): Query {
        this.string_query = this.create_table_query
        .replace("{table}", this.conn.table);
        return this;
    }

    truncateTable(): Query {
        this.string_query = this.truncate_table_query
        .replace("{table}", this.conn.table);
        return this;
    }

    compileQuery(): boolean {
        if (Object.keys(this.query).length === 0 && this.string_query !== "") {
            return true;
        } else {
            this.string_query = "";
            this.debug.log("query type", this.getQueryType(), "query");
            switch(this.getQueryType()) {
                case "select":
                this.debug.log("compile select", "", "query");
                this.compileSelect();
                this.compileFrom();
                this.compileWhere();
                this.compileOrder();
                this.compileLimit();
                break;
                case "insert":
                this.debug.log("compile insert", "", "query");
                this.compileSelect();
                this.compileInto();
                this.compileFrom();
                break;
                case "delete":
                this.debug.log("compile delete", "", "query");
                this.compileDelete();
                this.compileFrom();
                this.compileWhere();
                this.compileLimit();
                break;
                case "update":
                this.debug.log("compile update", "", "query");
                this.compileUpdate();
                this.compileWhere();
                this.compileLimit();
                break;
            }
            return true;
        }
    }

    compileUpdate(): boolean { // TODO safe
        let keys = Object.keys(this.query.set);
        let set: string = "";
        let i: number = 1;
        let keys_cnt: number = keys.length;
        let isArray_key: number = 0;
        let new_value;
        let prefix;
        let postfix;
        keys.forEach((key) => {
            isArray_key = 0;
            if (Array.isArray(this.query.set[key])) {
                this.params.unshift(this.query.set[key]);
                isArray_key = 1;
            }
            prefix = (i !== 1 ? " " : "");
            new_value = "";
            if(isArray_key) {
                new_value = "?";
            } else if(this.isNumber(this.query.set[key])) {
                new_value = +this.query.set[key];
            } else {
                new_value = "'" + this.addSlashes(this.query.set[key]) + "'";
            }
            key = this.safe(key);
            postfix = (keys_cnt > 1 && i < keys_cnt ? "," : "");
            set = set + prefix + key + " = " + new_value + postfix;
            i++;
        });
        this.string_query = this.update_query.replace("{table}", this.query.update).replace("{set}", set);
        return true;
    }

    addSlashes(value): any {
        this.debug.log("addslashes", value);
        this.debug.log("addslashes t", this.isString(value));
        if (!this.isString(value)) { return value; }
        if (value.indexOf("'") !== -1) {
            value = value.split("'").join("\\'");
            this.debug.log("addslashes r", value);
        }
        return value;
    }

    safe(value): any {
        if (checkSpecialWords(value)) {
            return "`" + value + "`";
        } else {
            return value;
        }
    }

    compileSelect(): boolean { // TODO safe
        let select = "";
        if(this.isString(this.query.select)) {
            select = this.query.select;
        } else if (Array.isArray(this.query.select)) {
            select = this.query.select.join(", ");
        } else if (typeof this.query.select === 'object') {
            let keys = Object.keys(this.query.select);
            let i: number = 1;
            let keys_cnt: number = keys.length;
            keys.forEach((key) => {
                let alias = this.query.select[key];
                if (alias === 1 || alias === true) {
                    select += key + (keys_cnt > 1 && i < keys_cnt ? "," : "");
                } else {
                    select += key + " as " + alias + (keys_cnt > 1 && i < keys_cnt ? "," : "");
                }
                i++;
            });
        }
        this.string_query = this.select_query.replace("{select}", select);
        return true;
    }

    isNumber(variable): boolean { // TODO to shared utils
        return typeof variable === "number";
    }

    isString(variable): boolean { // TODO to shared utils
        return typeof variable === "string" || variable instanceof String;
    }

    compileFrom(): boolean { // TODO aliasing, arrays, objects safe
        if (this.query.into !== undefined) {
            this.params = this.query.from;
            this.string_query += this.from_query.replace("{from}", "?");
            return true;
        }
        if (this.query.from === undefined) {
            return true;
        }
        let from = "";
        if (this.isString(this.query.from)) {
            from = this.query.from;
        }
        if (from !== "") {
            this.string_query += this.from_query.replace("{from}", from);
        }
        return true;
    }

    compileDelete(): boolean { // TODO aliasing, arrays, objects safe
        let delete_table = this.conn.table;
        if (this.query.delete !== undefined) {
            if (this.isString(this.query.delete)) {
                delete_table = this.query.delete;
            }
        }
        this.string_query = this.delete_query.replace("{table}", delete_table);
        return true;
    }

    compileInto(): boolean { // TODO aliasing, arrays, objects safe
        let into: string = "";
        if (this.query.into == undefined) {
            return true;
        }
        if (this.isString(this.query.into)) {
            into = this.query.into;
        }
        if(into !== "") {
            this.string_query += this.into_query.replace("{table}", into);
        }
        return true;
    }

    compileWhere(): boolean { // TODO arrays, params safe
        if (this.query.where === undefined) {
            return true;
        }
        let where: string = "";
        if (this.isString(this.query.where)) {
            where = this.query.where;
        } else if (typeof this.query.where === 'object') {
            let keys = Object.keys(this.query.where);
            let i: number = 1;
            let keys_cnt: number = keys.length;
            keys.forEach((key) => {
                let value = this.query.where[key];
                if (!this.isNumber(value)) {
                    value = "'" + value + "'";
                }
                where += key + " = " + value + (keys_cnt > 1 && i < keys_cnt ? " AND " : "");
                i++;
            });
        }
        if (where !== "") {
            this.string_query += this.where_query.replace("{where}", where);
        }
        return true;
    }

    compileOrder(): boolean { // TODO arrays params safe multiple
        if (this.query.order === undefined) {
            return true;
        }
        let order: string = "";
        if (this.isString(this.query.order)) {
            order += this.query.order;
        } else if (Array.isArray(this.query.order)) {
            order = this.query.order.join(" ");
        } else if (typeof this.query.order === 'object') {
            let keys = Object.keys(this.query.order);
            let i: number = 1;
            let keys_cnt: number = keys.length;
            keys.some((key) => {
                let type = this.query.order[key];
                order += key + " " + type;
                i++;
                return true;
            });
        }
        if (order !== "") {
            this.string_query += this.order_query.replace("{order}", order);
        }
        return true;
    }

    compileLimit(): boolean { // TODO aliasing, arrays, objects, safe, multiple
        if (this.query.limit === undefined) {
            return true;
        }
        let limit: string = "";
        if (this.isString(this.query.limit) || this.isNumber(this.query.limit)) {
            limit = this.query.limit;
        }
        if (limit !== "") {
            this.string_query += this.limit_query.replace("{limit}", limit);
        }
        return true;
    }

    getQueryType(): string {
        if (this.query.into !== undefined) {
            return "insert";
        }
        if (this.query.select !== undefined) {
            return "select";
        }
        if (this.query.update !== undefined) {
            return "update";
        }
        if (this.query.delete !== undefined) {
            return "delete";
        }
    }

    getQuery(object: number = 0) {
        this.compileQuery();
        if (this.compileQuery()) {
            if (object) {
                return this.query;
            } else {
                return this.string_query;
            }
        }
    }

    showTables() {
        this.string_query = this.show_tables_query;
        return this;
    }

    showDatabases() {
        this.string_query = this.show_databases_query;
        return this;
    }

    exec() {
        if (this.compileQuery()) {
            this.debug.log("query sql debug: ", [this.query, this.string_query, this.params], "query");
            this.debug.log("query sql:", this.string_query, "query");
            this.debug.log("query sql params:", this.params, "query");
            let result = alasql(this.string_query, this.params);
            this.debug.log("query sql result:", result, "query");
            this.query = {};
            this.string_query = "";
            this.params = [];
            return result;
        } else {
            return false;
        }
    }

    one() {
        this.query.limit = 1;
        let result = this.exec();
        if (result) {
            return result[0];
        }
        return result;
    }

    all() {
        this.query.limit = undefined;
        return this.exec();
    }

}
