import { Injectable } from "@angular/core";

import { Query } from "../models/query";

import { DebugService } from "../services/debug.service";

@Injectable()
export class Model {
    private table: string;
    private debug: DebugService;
    private query: Query;

    init(table: string, debug: DebugService, query: Query) {
        this.table = table;
        this.debug = debug;
        this.query = query;
        this.query.setTable(this.table);
        this.debug.log("model", this.table + " constructor", "model");
        this.debug.log("model check table", this.checkTable(), "model");
        if (!this.checkTable()) {
            this.createTable();
        }
    }

    truncateTable(): boolean {
        let result = this.query.truncateTable().exec();
        this.debug.log("model " + this.table + " truncate tbl result", result, "model");
        return result;
    }

    createTable(): boolean {
        let result = this.query.createTable().exec();
        this.debug.log("model " + this.table + " create tbl result", result, "model");
        return result;
    }

    checkTable(): boolean {
        let tables = this.query.showTables().exec();
        let found: boolean = false;
        this.debug.log("model " + this.table + " check tbl", tables, "model");
        tables.forEach((table: any) => {
            if (table.tableid === this.table) {
                found = true;
            }
        });
        this.debug.log("model " + this.table + " check found", found, "model");
        return found;
    }

    insert(new_model): boolean {
        let result = this.query.select().into(this.table).from([[new_model]]).exec();
        this.debug.log("model " + this.table + " insert result", result, "model");
        return result;
    }

    getLastInsertedId(): number {
        let result = this.query.select({"LAST(id)": "lid"}).from(this.table).exec();
        if (result[0].lid !== undefined) {
            this.debug.log("model " + this.table + " get last id", result[0].lid, "model");
            return result[0].lid;
        }
        return 0;
    }

    update(id: number, diff): boolean {
        let result: any;
        let update_date = +new Date();
        diff.update_date = update_date;
        result = this.query.update(this.table).set(diff).where({id: +id}).exec();
        this.debug.log("model", this.table + " update", "model");
        return result;
    }

    find(condition: any = false): any {
        let model: any;
        if (condition !== false) {
            if(this.query.isNumber(condition)) {
                condition = {id: condition};
            }
            model = this.query.select().from(this.table).where(condition).limit(1);
        } else {
            model = this.query.select().from(this.table).limit(1);
        }
        this.debug.log("model " + this.table + " find", condition, "model");
        return model;
    }

    delete(condition): Query {
        if(this.query.isNumber(condition)) {
            condition = {id: condition};
        }
        this.debug.log("model " + this.table + " delete", condition, "model");
        return this.query.delete(this.table).where(condition);
    }

}
