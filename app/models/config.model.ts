import { Injectable } from "@angular/core";

import { Query } from "./query";
import { Model } from "./model";

import { DebugService } from "../services/debug.service";

export class Config extends Model {
    constructor() {
        super();
        let debug = new DebugService;
        super.init("config", debug, new Query(debug));
    }
}
