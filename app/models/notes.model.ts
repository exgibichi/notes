import { Injectable } from "@angular/core";

import { Query } from "./query";
import { Model } from "./model";

import { DebugService } from "../services/debug.service";

export class Notes extends Model {
    constructor() {
        super();
        let debug = new DebugService;
        super.init("notes", debug, new Query(debug));
    }
}
