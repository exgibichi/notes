import { Injectable } from "@angular/core";

import { Query } from "./query";
import { Model } from "./model";

import { DebugService } from "../services/debug.service";

export class Categories extends Model {
    constructor() {
        super();
        let debug = new DebugService;
        super.init("categories", debug, new Query(debug));
    }
}
