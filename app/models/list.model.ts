import { Injectable } from "@angular/core";

import { Query } from "./query";
import { Model } from "./model";

import { DebugService } from "../services/debug.service";

export class List extends Model {
    constructor() {
        super();
        let debug = new DebugService;
        super.init("lists", debug, new Query(debug));
    }
}
