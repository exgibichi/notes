import { Headers, Http } from "@angular/http";

import { DebugService } from "../services/debug.service";

import "rxjs/add/operator/toPromise";

export class Dropbox {
    auth_url: string = "https://www.dropbox.com/oauth2/authorize";
    api_url = {
        "api": "https://api.dropboxapi.com/2/",
        "content": "https://content.dropboxapi.com/2/"
    };
    response_type: string = "token";
    client_id: string;
    debug: DebugService;
    http: Http;
    token: string = "";
    state: string = "dbox";
    headers: any = {};
    methods = {
        "auth_check": {
            "method": "POST",
            "url": "users/get_account",
            "api_url": "api"
        },
        "upload": {
            "method": "POST",
            "url": "files/upload",
            "api_url": "content"
        },
        "file_list": {
            "method": "POST",
            "url": "files/list_folder",
            "api_url": "api"
        },
        "download": {
            "method": "POST",
            "url": "/files/download",
            "api_url": "content"
        }
    };

    init(debug, http, client_id: string, token?) {
        this.debug = debug;
        this.client_id = client_id;
        this.http = http;
        if (token) {
            this.token = token;
        }
    }

    checkAuth(account_id) {
        this.headers = this.getDefaultHeader();
        let request_url = this.compileApiUrl(this.methods["auth_check"]);
        let headers = this.compileHeaders(this.token);
        let data = this.compileData({account_id: account_id});
        return this.request(request_url, headers, data, this.methods["auth_check"]["method"])
        .then(result => {
            this.debug.log("result", result);
            if (result.email !== undefined) {
                return 1;
            }
        });
    }

    download(file_path: string, id?: string) {
        this.headers = this.getDefaultHeader();
        let data = null;
        if (id) {
            this.headers["Dropbox-API-Arg"] = JSON.stringify({
                "path": id
            });
        }
        if (file_path !== "") {
            this.headers["Dropbox-API-Arg"] = JSON.stringify({
                "path": file_path
            });
        }
        this.headers["Content-Type"] = "";
        let request_url = this.compileApiUrl(this.methods["download"]);
        let headers = this.compileHeaders(this.token);
        return this.request(request_url, headers, data, this.methods["download"]["method"]).then(file => {
            this.debug.log("file", file);
            return file;
        });
    }

    getFileList() {
        this.headers = this.getDefaultHeader();
        let data = this.compileData({
            "path": "",
            "recursive": false,
            "include_media_info": false,
            "include_deleted": false,
            "include_has_explicit_shared_members": false
        });
        let request_url = this.compileApiUrl(this.methods["file_list"]);
        let headers = this.compileHeaders(this.token);
        return this.request(request_url, headers, data, this.methods["file_list"]["method"]).then(files => {
            if (files.entries !== undefined) {
                this.debug.log("entries", files.entries);
                return files.entries;
            }
        });
    }

    upload(file, file_data) {
        this.headers = this.getDefaultHeader();
        this.headers["Content-Type"] = "application/octet-stream";
        this.headers["Dropbox-API-Arg"] = JSON.stringify({
            "path": "/" + file,
            "mode": "add",
            "autorename": true,
            "mute": false
        });
        let request_url = this.compileApiUrl(this.methods["upload"]);
        let headers = this.compileHeaders(this.token);
        let data = file_data;
        return this.request(request_url, headers, data, this.methods["upload"]["method"])
        .then(result => {
            this.debug.log("up_result", result);
            if (result.name === file) {
                return true;
            }
        });
    }

    compileHeaders(token): any {
        let headers = {};
        this.headers["Authorization"] = this.headers["Authorization"].replace("{token}", token);
        Object.keys(this.headers).forEach((header) => {
            if (this.headers[header] !== "") {
                headers[header] = this.headers[header];
            }
        });
        return new Headers(headers);
    }

    compileData(data: any): string {
        return JSON.stringify(data);
    }

    request(url: string, headers: Headers, data?: any, method: string = "GET"): Promise<any> {
        let request;
        this.debug.log("dbox request url: ", url);
        this.debug.log("dbox request headers: ", headers);
        this.debug.log("dbox request data: ", data);
        this.debug.log("dbox request method: ", method);
        if (method === "POST") {
            request = this.http.post(url, data, {headers: headers});
        }
        if (method === "GET") {
            request = this.http.get(url, {headers: headers});
        }
        return request
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log("An error occurred", error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getDefaultHeader() {
        return {"Content-Type": "application/json", "Authorization": "Bearer {token}"};
    }

    compileApiUrl(method): string {
        return this.api_url[method["api_url"]] + method["url"];
    }

    compileAuthUrl(client_id: string, redirect_uri: string): string {
        return this.auth_url +
        "?response_type=" +
        this.response_type +
        "&client_id=" +
        client_id +
        "&redirect_uri=" +
        redirect_uri +
        "&state=" + this.state;
    }
}
