import { Directive, Input, ElementRef } from "@angular/core";

@Directive({
    selector: "[focus]"
})
export class FocusDirective {
    @Input() focus: boolean;
    private element: HTMLElement;

    constructor ($element: ElementRef) {
        this.element = $element.nativeElement;
    }

    ngAfterContentChecked(): void {
        this.giveFocus();
    }

    giveFocus(): void {
        if (this.focus) {
            this.element.focus();
        }
    }
}
