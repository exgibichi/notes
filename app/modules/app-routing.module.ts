import { NgModule }             from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ListsComponent }   from "../components/lists.component";
import { TodosComponent }      from "../components/todos.component";
import { NotesComponent }  from "../components/notes.component";
import { ExportComponent }  from "../components/export.component";
import { ImportComponent }  from "../components/import.component";
import { AuthComponent }  from "../components/auth.component";
import { ConfigComponent }  from "../components/config.component";
import { SettingsComponent }  from "../components/settings.component";

const routes: Routes = [
  { path: "", redirectTo: "/lists", pathMatch: "full" },
  { path: "lists", component: ListsComponent },
  { path: "lists/:category_id", component: ListsComponent },
  { path: "todos", component: TodosComponent },
  { path: "todos/:category_id", component: TodosComponent },
  { path: "notes", component: NotesComponent },
  { path: "notes/:category_id", component: NotesComponent },
  { path: "notes/:note_id", component: NotesComponent },
  { path: "export", component: ExportComponent },
  { path: "import", component: ImportComponent },
  { path: "auth", component: AuthComponent },
  { path: "app_config", component: ConfigComponent },
  { path: "user_settings", component: SettingsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
