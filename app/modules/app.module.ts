import { NgModule }      from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule }   from "@angular/forms";
import { HttpModule }    from "@angular/http";
import { AppRoutingModule } from "./app-routing.module";
import { SimpleNotificationsModule } from "angular2-notifications";

import { AppComponent }         from "../components/app.component";
import { ListsComponent }   from "../components/lists.component";
import { TodosComponent }      from "../components/todos.component";
import { NotesComponent }  from "../components/notes.component";
import { MainMenuComponent }  from "../components/main-menu.component";
import { AuthComponent }  from "../components/auth.component";
import { CategoriesComponent }  from "../components/categories.component";
import { ExportComponent }  from "../components/export.component";
import { ImportComponent }  from "../components/import.component";
import { ConfigComponent }  from "../components/config.component";
import { SettingsComponent }  from "../components/settings.component";

import { CategoriesService }  from "../services/categories.service";
import { ListService }  from "../services/list.service";
import { AuthService }  from "../services/auth.service";
import { TodoService }  from "../services/todo.service";
import { NoteService }  from "../services/note.service";
import { ConfigService }  from "../services/config.service";
import { SettingsService }  from "../services/settings.service";
import { DebugService }  from "../services/debug.service";

import { Query }  from "../models/query";
import { Model }  from "../models/model";
import { Categories }  from "../models/categories.model";
import { List }  from "../models/list.model";
import { Auth }  from "../models/auth.model";
import { Todo }  from "../models/todo.model";
import { SubTodo }  from "../models/subtodo.model";
import { Notes }  from "../models/notes.model";
import { Config }  from "../models/config.model";
import { Settings }  from "../models/settings.model";

import { Dropbox }  from "../libs/dropbox";
import { FileSizePipe } from "../pipes/file-size.pipe";
import { KeysPipe } from "../pipes/keys.pipe";

import { FocusDirective } from "../directives/focus.directive";


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        SimpleNotificationsModule
    ],
    declarations: [
        AppComponent,
        ListsComponent,
        TodosComponent,
        NotesComponent,
        AuthComponent,
        CategoriesComponent,
        MainMenuComponent,
        ExportComponent,
        ImportComponent,
        ConfigComponent,
        SettingsComponent,
        FileSizePipe,
        KeysPipe,
        FocusDirective
    ],
    providers: [
        Query,
        Model,
        Categories,
        Auth,
        List,
        Todo,
        SubTodo,
        Notes,
        Config,
        Settings,
        CategoriesService,
        ListService,
        AuthService,
        TodoService,
        NoteService,
        ConfigService,
        SettingsService,
        DebugService,
        Dropbox
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule { }
