import { Injectable } from "@angular/core";

import { Tasks } from "../types/tasks";
import { SubTasks } from "../types/subtasks";
import { Todo } from "../models/todo.model";
import { SubTodo } from "../models/subtodo.model";

import { ItemsService } from "./items.service";

@Injectable()
export class TodoService extends ItemsService {

    constructor(model: Todo, private sub_model: SubTodo) {
        super();
        super.init(model);
    }

    createSub(text: string, parent_id: number, urls?: Array<string>): boolean|number {
        let time = +new Date();
        let id = this.getNewSubId();
        let urls_arr = [];
        if (urls) {
            urls_arr = urls;
        }
        let new_sub_todo: SubTasks = {
            id: id,
            parent_id: parent_id,
            text: text,
            type: "new",
            prior: 0,
            urls: urls_arr,
            create_date: time,
            update_date: time
        };
        if(this.sub_model.insert(new_sub_todo)) {
            return id;
        } else {
            return false;
        }
    }

    getNewSubId(): number {
        let new_id: number = 1;
        let last_id: number = this.sub_model.getLastInsertedId();
        if (last_id) {
            new_id = +last_id + 1;
        }
        return new_id;
    }

    getSubById(item_id: number) {
        return this.sub_model.find(+item_id).one();
    }

    create(text: string, category_id: number, urls?: Array<string>): boolean {
        let time = +new Date();
        let id = this.getNewId();
        let urls_arr = [];
        if (urls) {
            urls_arr = urls;
        }
        let new_todo: Tasks = {
            id: id,
            category_id: category_id,
            text: text,
            type: "new",
            prior: 0,
            urls: urls_arr,
            create_date: time,
            update_date: time
        };
        return this.model.insert(new_todo);
    }

    getItemsByCategoryId(id: number, type: string, orderBy?, orderType?): Tasks[] {
        return super.getItemsByCategoryId(id, type, orderBy, orderType);
    }

    saveSub(id: number, new_value: string, urls?: Array<string>): boolean {
        return this.sub_model.update(+id, {text: new_value, urls: urls});
    }

    save(id: number, new_value: string, urls?: Array<string>): boolean {
        return this.model.update(+id, {text: new_value, urls: urls});
    }

}
