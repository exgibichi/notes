import { Injectable } from "@angular/core";

import { Settings } from "../models/settings.model";

import { DebugService } from "../services/debug.service";

@Injectable()
export class SettingsService {
    values = {};
    private model: Settings;
    constructor(private debug: DebugService) {}

    init() {
        this.model = new Settings;
        this.loadAll();
        if (Object.keys(this.values).length === 0) {
            this.setDefault();
        }
    }

    setDefault(): void {
        this.set("background image",  "none"); // todo add setting type boolean => checkbox
        this.set("lock app with password when not in focus",  "0");
    }

    create(name: string, value: string): boolean {
        let time = +new Date();
        let id: number = this.getNewId();
        let new_value = {
            id: id,
            name: name,
            value: value,
            create_date: time,
            update_date: time
        };
        return this.model.insert(new_value);
    }

    getNewId(): number {
        let new_id: number = 1;
        let last_id: number = this.model.getLastInsertedId();
        if (last_id) {
            new_id = last_id + 1;
        }
        return new_id;
    }

    set(name: string, value: any): boolean {
        if (this.values[name] !== undefined) {
            let new_value = this.model.find({name: name}).one();
            if (this.model.update(+new_value.id, {value: value})) {
                this.values[name] = value;
                return true;
            }
        } else {
            if (this.create(name, value)) {
                this.values[name] = value;
                return true;
            }
        }
        return false;
    }

    get(name: string): any { // TODO add type config
        return this.values[name];
    }

    truncate(): void {
        if (this.model.truncateTable()) {
            this.values = [];
            this.setDefault();
            this.loadAll();
        }
    }

    loadAll(): any {
        this.debug.log("settings", "loadAll");
        this.model.find().all().forEach(value => {
            this.values[value.name] = value.value;
        });
    }

    getAll(): any {
        return this.values;
    }
}
