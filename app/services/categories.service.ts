import { Injectable } from "@angular/core";

import { Category } from "../types/category";
import { Categories } from "../models/categories.model";

import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class CategoriesService {
    private _update = new BehaviorSubject<number>(0);
    public is_update = this._update.asObservable();
    type;

    constructor(private model: Categories) {}

    create(type: string, name: string): boolean {
        let time = +new Date();
        let id: number = this.getNewId();
        let new_category: Category = {
            id: +id,
            cnt: 0,
            acnt: 0,
            type: type,
            name: name,
            prior: 1,
            create_date: time,
            update_date: time
        };
        if (this.model.insert(new_category)) {
            this._update.next(1);
            return true;
        }
    }

    getLastInsertedId() {
        return this.model.getLastInsertedId();
    }

    getNewId(): number {
        let new_id: number = 1;
        let last_id: number = this.model.getLastInsertedId();
        if (last_id) {
            new_id = +last_id + 1;
        }
        return new_id;
    }

    update(id: number, new_category: any): boolean {
        if (this.model.update(+id, new_category)) {
            this._update.next(1);
            return true;
        }
    }

    deleteById(id: number): boolean {
        if (this.model.delete(+id).exec()) {
            this._update.next(1);
            return true;
        }
    }

    changePrior(id, up): boolean {
        let model = this.model.find(+id).one();
        if (model.prior != undefined) {
            let prior = +model.prior;
            model.prior = up ? prior+1 : prior-1;
        } else {
            model.prior = up ? 1 : -1;
        }
        if (this.model.update(+id, model)) {
            this._update.next(1);
            return true;
        }
        return false;
    }

    getById(id: number): Category {
        return this.model.find(+id).one();
    }

    getCategories(type): Category[] {
        this.type = type;
        return this.model.find({type: type}).order({prior: "DESC"}).all();
    }
}
