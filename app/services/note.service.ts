import { Injectable } from "@angular/core";

import { NoteEntry } from "../types/note-entry";
import { Notes } from "../models/notes.model";

import { ItemsService } from "./items.service";

@Injectable()
export class NoteService extends ItemsService {

    constructor(
        model: Notes,
    ) {
        super();
        super.init(model);
    }

    create(title: string, text: string, category_id: number, urls?: Array<string>): boolean {
        let id = this.getNewId();
        let time = +new Date();
        let urls_arr = [];
        if (urls) {
            urls_arr = urls;
        }
        let new_note = {
            id: id,
            category_id: category_id,
            title: title,
            text: text,
            urls: urls_arr,
            create_date: time,
            update_date: time
        };
        return this.model.insert(new_note);
    }

    getItemsByCategoryId(id: number, orderBy?, orderType?): NoteEntry[] {
        return super.getItemsByCategoryId(id, null, orderBy, orderType);
    }

    save(id: number, new_title: string, new_text: string, urls?: Array<string>): boolean {
        return this.model.update(+id, {title: new_title, text: new_text, urls: urls});
    }

}
