import { Injectable } from "@angular/core";

@Injectable()
export class CacheService {
    private storage;

    constructor() {
        this.storage = {};
    }

    set(name: string, value: any): void {
        this.storage[name] = value;
    }

    get(name: string) {
        return this.storage[name];
    }
}
