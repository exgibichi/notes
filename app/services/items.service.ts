export class ItemsService {
    protected model;

    init(model) {
        this.model = model;
    }

    getNewId(): number {
        let new_id: number = 1;
        let last_id: number = this.model.getLastInsertedId();
        if (last_id) {
            new_id = +last_id + 1;
        }
        return new_id;
    }

    getItemsByCategoryId(id: number, type?, order_by?, order_type?): any[] {
        let condition;
        condition = {category_id: +id};
        if (type) {
            condition = {category_id: +id, type: type};
        }
        let items = this.model.find(condition);
        if (order_by && order_type) {
            items = items.order([order_by, order_type]);
        }
        return items.all();
    }

    getById(id: number): any {
        return this.model.find(+id).one();
    }

    remove(id: number): boolean {
        return this.model.delete(+id).exec();
    }

    removeByCategoryId(category_id: number): boolean {
        return this.model.delete({category_id: +category_id}).all();
    }

    changeType(item_id, type): boolean {
        return this.model.update(item_id, {type: type});
    }

    changePrior(item_id, up): boolean {
        let model = this.model.find(+item_id).one();
        let prior;
        if (model.prior != undefined) {
            prior = +model.prior;
            prior = up ? prior+1 : prior-1;
        } else {
            prior = up ? 1 : -1;
        } 
        return this.model.update(+item_id, {prior: prior});
    }

    transfer(item_id, category_id): boolean {
        return this.model.update(+item_id, {category_id: +category_id});
    }

    getItems(condition?: any): any[] {
        if (condition) {
            return this.model.find(condition).all();
        }
        return this.model.find().all();
    }
}
