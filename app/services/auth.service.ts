import { Injectable }    from "@angular/core";

import { Auth } from "../models/auth.model";

import { ItemsService } from "./items.service";

@Injectable()
export class AuthService extends ItemsService { // todo check what extends

    constructor(model: Auth) {
        super();
        super.init(model);
    }

    setExpired(name: string): boolean {
        let model = this.model.find({name: name}).one();
        return this.model.update(+model.id, {expired: 1});
    }

    checkToken(name: string): boolean {
        let model = this.model.find({name: name}).one();
        return model as boolean;
    }

    update(name: string, token: string, uid: string, expired: number = 0): boolean { // TODO after add type add boolean type for expired
        let model = this.model.find({name: name}).one();
        return this.model.update(+model.id, {name: name, token: token, uid: uid, expired: expired});
    }

    get(name: string): any { // TODO add type
        let model = this.model.find({name: name}).one();
        return model;
    }

    addToken(name: string, token: string, uid: string): boolean {
        let time = +new Date();
        let id = this.getNewId();
        let new_auth = {
            id: +id,
            name: name,
            token: token,
            uid: uid,
            expired: 0,
            create_date: time,
            update_date: time
        };
        return this.model.insert(new_auth);
    }

}
