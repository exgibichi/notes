import { Injectable } from "@angular/core";

@Injectable()
export class DebugService {
    use: boolean = true;
    log(msg: string, value: any = "", type: string = "app"): void {
        if (this.use) {
            console.log(msg, value);
        }
    }
}
