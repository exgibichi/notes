import { Injectable } from "@angular/core";

import { ListEntry } from "../types/list-entry";
import { List } from "../models/list.model";

import { ItemsService } from "./items.service";

@Injectable()
export class ListService extends ItemsService {

    constructor(model: List) {
        super();
        super.init(model);
    }

    create(text: string, category_id: number, urls?: Array<string>): boolean {
        let time = +new Date();
        let id = this.getNewId();
        let urls_arr = [];
        if (urls) {
            urls_arr = urls;
        }
        let new_list: ListEntry = {
            id: id,
            category_id: category_id,
            text: text,
            urls: urls_arr,
            prior: 1,
            create_date: time,
            update_date: time
        };
        return this.model.insert(new_list);
    }

    getItemsByCategoryId(id: number, orderBy?, orderType?): ListEntry[] {
        return super.getItemsByCategoryId(id, null, orderBy, orderType);
    }

    save(id: number, new_value: string, urls?: Array<string>): boolean { // todo to abstract service
        return this.model.update(+id, {text: new_value, urls: urls});
    }

}
