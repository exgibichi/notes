export class NoteEntry {
    id: number;
    category_id: number;
    text: string;
    title: string;
    urls: Array<string>;
    create_date: number;
    update_date: number;
}
