export class Category {
    id: number;
    type: string;
    name: string;
    prior: number;
    create_date: number;
    update_date: number;
    cnt: number;
    acnt: number;
}
