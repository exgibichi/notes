export class SubTasks {
    id: number;
    parent_id: number;
    text: string;
    type: string;
    urls: Array<string>;
    prior: number;
    create_date: number;
    update_date: number;
}
