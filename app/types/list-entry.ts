export class ListEntry {
    id: number;
    category_id: number;
    text: string;
    urls: Array<string>;
    prior: number;
    create_date: number;
    update_date: number;
}
