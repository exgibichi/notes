export class Links {
    id: number;
    text: string;
    url: string;
}
