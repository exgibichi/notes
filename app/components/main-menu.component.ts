import { Component, OnInit } from "@angular/core";

import { Links } from "../types/links";

@Component({
    moduleId: module.id,
    selector: "[main-menu]",
    templateUrl: "../views/main-menu.component.html"
})

export class MainMenuComponent implements OnInit {
    links: Links[] = [];
    main_link: Links;

    ngOnInit(): void {
        this.links = this.getLinks();
        this.main_link = this.getMainLink();
    }

    getMainLink(): Links {
        let link: Links = {id: 0, text: "", url: "/lists"};
        return link;
    }

    getLinks(): Links[] {
        let links: Links[] = [
            {id: 1, text: "Lists", url: "/lists"},
            {id: 2, text: "Todos", url: "/todos"},
            {id: 3, text: "Notes", url: "/notes"},
            {id: 4, text: "Export", url: "/export"},
            {id: 5, text: "Import", url: "/import"},
            {id: 6, text: "Auth", url: "/auth"},
            {id: 7, text: "App config", url: "/app_config"},
            {id: 8, text: "User settings", url: "/user_settings"}
        ];
        return links;
    }
}
