import { Component } from "@angular/core";
import { Http } from "@angular/http";
import { Router } from "@angular/router";

import { Dropbox } from "../libs/dropbox";

import { AuthService } from "../services/auth.service";
import { ConfigService } from "../services/config.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";

declare let base64: any;

@Component({
    moduleId: module.id,
    templateUrl: "../views/import.component.html"
})

export class ImportComponent {
    title = "Import data";
    file_info;
    file_list;

    constructor(
        private dropbox: Dropbox,
        private http: Http,
        private router: Router,
        private auth_service: AuthService,
        private config: ConfigService,
        private debug: DebugService,
        private alert: NotificationsService
    ) {
        let model = this.auth_service.get("dbox");
        let dropbox_app_key = this.config.get("dropbox_app_key");
        if (model) {
            this.dropbox.init(this.debug, this.http, dropbox_app_key, model.token);
        }
    }

    importFile(event): void {
        let file = event.target.files[0];
        if (file) {
            this.file_info = {
                "name": file.name,
                "size": file.size,
                "type": file.type,
                "last": file.lastModified,
                "handler": file
            };
        } else {
            this.debug.log("Failed to load file");
        }
    }

    importDbox(file_id?) {
        if (file_id) {
            this.dropbox.download(file_id).then(file => {
                this.debug.log("dfile", file);
                if (this.updateData(file)) {
                    this.alert.success("Success", "DB imported succefuly");
                } else {
                    this.alert.error("Error", "Cant import db");
                }
            });
        } else {
            this.dropbox.getFileList().then((files: any) => {
                this.debug.log("files", files);
                this.file_list = files;
            });
        }
    }

    importFromFile(event) {
        let contents = event.currentTarget.result;
        contents = JSON.parse(contents);
        if (this.updateData(contents)) {
            this.alert.success("Success", "DB imported succefuly");
        } else {
            this.alert.error("Error", "Cant import db");
        }
    }

    updateData(contents): boolean { // todo validation
        let data = contents;
        let fields = JSON.parse(base64.decode(data.data));
        Object.keys(fields).forEach(field_name => {
            if (field_name.indexOf("_hash") === -1) {
                this.debug.log("import: " + field_name);
                localStorage.setItem(field_name, base64.decode(fields[field_name]));
            }
        });
        this.debug.log("imported");
        return true;
    }

    import(file) {
        let reader = new FileReader();
        reader.onloadend = (event) => {
            this.importFromFile(event);
        };
        reader.readAsText(file);
    }
}
