import { Component } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";

import { Category } from "../types/category";

import { CategoriesService } from "../services/categories.service";
import { ListService } from "../services/list.service";
import { TodoService } from "../services/todo.service";
import { NoteService } from "../services/note.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";
import { SettingsService } from "../services/settings.service";

@Component({
    moduleId: module.id,
    selector: "[categories]",
    templateUrl: "../views/categories.component.html"
})

export class CategoriesComponent {
    types = [
        "lists",
        "todos",
        "notes"
    ];
    categories: Category[];
    category: Category;
    current_type: string;
    selected_category_id;
    edit_mode = 0;

    constructor(
        private router: Router,
        private categories_service: CategoriesService,
        private debug: DebugService,
        private list_service: ListService,
        private todo_service: TodoService,
        private note_service: NoteService,
        private alert: NotificationsService,
        private settings: SettingsService
    ) {
        this.router.events.subscribe((new_path: NavigationEnd) => {
            if (!new_path.urlAfterRedirects) { return; }
            let current_path = new_path.urlAfterRedirects;
            let path_parts = current_path.split("/");
            let current_type: string = "";
            if (path_parts[1] !== undefined) {
                current_type = path_parts[1];
            }
            let saved_category_id = this.settings.get("saved_" + current_type + "_category");
            if (path_parts[2] !== undefined) {
                this.selected_category_id = +path_parts[2];
                if (saved_category_id !== this.selected_category_id) {
                    this.settings.set("saved_" + current_type + "_category", this.selected_category_id);
                }
            } else {
                this.selected_category_id = 0;
                if (this.current_type !== current_type) {
                    if (saved_category_id) {
                        this.debug.log("navigate to saved category");
                        this.router.navigate(["/" + current_type + "/" + saved_category_id]);
                    }
                }
            }
            if (this.current_type === current_type) { return; }
            this.current_type = current_type;
            if (this.types.indexOf(current_type) !== -1) {
                this.updateCategories(current_type);
            } else {
                this.hideCategories();
            }
        });
        this.categories_service.is_update
        .subscribe(item => {
            if (item === 1) {
                this.updateCategories(this.current_type);
            }
        });
    }

    remove(id: number): void {
        if (!confirm("Delete category?")) { return; }
        id = +id;
        if (this.categories_service.deleteById(id)) {
            this.alert.success("Success", "Category removed succefuly");
            if (this.current_type === "lists") {
                this.list_service.removeByCategoryId(id);
            }
            if (this.current_type === "todos") {
                this.todo_service.removeByCategoryId(id);
            }
            if (this.current_type === "notes") {
                this.note_service.removeByCategoryId(id);
            }
            if (id === this.selected_category_id) {
                this.debug.log("id === this.selected_category_id", 1)
                this.selected_category_id = undefined;
                this.router.navigate(["/" + this.current_type]);
            }
        } else {
            this.alert.error("Error", "Cant remove category");
        }
    }

    changePriorItem(item_id, up): void {
        if (!item_id) { return; }
        if (this.categories_service.changePrior(+item_id, up)) {
            this.alert.success("Success", "Prior changed succefully");
        } else {
            this.alert.error("Error", "Cant change prior");
        }
    }

    add(name: string): void {
        name = name.trim();
        if (!name) { return; }
        if (this.categories_service.create(this.current_type, name)) {
            this.alert.success("Success", "Category added succefuly");
            let new_cat_id = this.categories_service.getLastInsertedId();
            if (new_cat_id) {
                this.router.navigate(["/" + this.current_type + "/" + new_cat_id]);
            }
        } else {
            this.alert.error("Error", "Cant add category");
        }
    }

    edit(id: number): void {
        if (!id) { return; }
        this.category = this.categories_service.getById(+id);
    }

    cancel(): void {
        this.category = undefined;
    }

    editMode(): void {
        this.edit_mode = this.edit_mode ? 0 : 1;
    }

    save(category_id: Category, new_name: string): void {
        if (!category_id) { return; }
        if (this.categories_service.update(+category_id, {name: new_name})) {
            this.alert.success("Success", "Category saved succefuly");
            this.category = undefined;
        } else {
            this.alert.error("Error", "Cant save category");
        }
    }

    updateCategories(type): void {
        this.debug.log("catz get cats for " + type);
        this.categories = this.categories_service.getCategories(type);
    }

    hideCategories(): void {
        this.debug.log("catz hide");
        this.categories = [];
        this.selected_category_id = undefined;
        this.current_type = undefined;
    }
}
