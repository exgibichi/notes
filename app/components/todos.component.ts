import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { Tasks } from "../types/tasks";

import { CategoriesService } from "../services/categories.service";
import { TodoService } from "../services/todo.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";
import { SettingsService } from "../services/settings.service";

import { ItemsComponent } from "./items.component";

@Component({
    moduleId: module.id,
    templateUrl: "../views/todos.component.html"
})

export class TodosComponent extends ItemsComponent implements OnInit {
    title = "Todos";
    item: Tasks;
    item_name = "task";
    items: Tasks[] = [];
    second_items: Tasks[] = [];
    types: string[] = ["new", "completed", "both"];
    sub: boolean = false;

    constructor(
        protected categories_service: CategoriesService,
        protected todo_service: TodoService,
        protected route: ActivatedRoute,
        protected debug: DebugService,
        protected alert: NotificationsService,
        protected settings: SettingsService,
    ) {
        super(categories_service, todo_service, route, debug, alert, settings);
    }

    ngOnInit(): void {
        super.init();
    }

    complete(item_id: number): void {
        super.changeType(item_id, this.types[1], "completed", "complete");
    }

    undo(item_id: number): void {
        super.changeType(item_id, this.types[0], "uncompleted", "uncomplete");
    }

    createSubItem(item_id: number): void {
        let sub_id = this.todo_service.createSub("sub task text", item_id);
        if(sub_id) {
            this.item = this.todo_service.getSubById(+sub_id);
            this.add_or_edit = true;
            this.sub = true;
        }
    }

    saveItem(item: any, new_title: string): void {
        let urls;
        if (!item) { return; }
        urls = this.extractUrls(new_title);
        this.debug.log("edit urls", urls);
        let save = this.sub ? this.todo_service.saveSub(item.id, new_title, urls) : this.todo_service.save(item.id, new_title, urls);
        if (save) {
            this.alert.success("Success", this.item_name + " saved succefuly");
            this.items = this.getOrderedItems(0);
            this.item = undefined;
            this.add_or_edit = false;
            this.sub = false;
        } else {
            this.alert.error("Error", "Cant save " + this.item_name);
        }
    }

    cancelItemEdit(): void {
        this.item = undefined;
        this.add_or_edit = false;
        this.sub = false;
    }

}
