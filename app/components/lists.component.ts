import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ListEntry } from "../types/list-entry";

import { CategoriesService } from "../services/categories.service";
import { ListService } from "../services/list.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";
import { SettingsService } from "../services/settings.service";

import { ItemsComponent } from "./items.component";

@Component({
    moduleId: module.id,
    templateUrl: "../views/lists.component.html"
})

export class ListsComponent extends ItemsComponent implements OnInit {
    title = "Lists";
    item: ListEntry;
    item_name = "entry";
    items: ListEntry[] = [];

    constructor(
        protected categories_service: CategoriesService,
        protected items_service: ListService,
        protected route: ActivatedRoute,
        protected debug: DebugService,
        protected alert: NotificationsService,
        protected settings: SettingsService,
    ) {
        super(categories_service, items_service, route, debug, alert, settings);
    }

    ngOnInit(): void {
        super.init();
    }

    zipItem(item_id: number): void {
        super.changeType(item_id, this.types[1], "zipped", "zip");
    }

    unzipItem(item_id: number): void {
        super.changeType(item_id, this.types[0], "unzipped", "unzip");
    }

}
