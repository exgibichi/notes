import { Component } from "@angular/core";

import { DebugService } from "../services/debug.service";
import { ConfigService } from "../services/config.service";
import { SettingsService } from "../services/settings.service";
import { Query } from "../models/query";

@Component({
    moduleId: module.id,
    selector: "my-app",
    templateUrl: "../views/app.component.html"
})
export class AppComponent {
    db: string = "notes";
    storage: string = "LOCALSTORAGE";
    bg_style = {};
    public options = {
        position: ["bottom", "right"],
        timeOut: 5000,
        lastOnBottom: true
    };

    constructor(
        private debug: DebugService,
        private config: ConfigService,
        private settings: SettingsService,
        private query: Query,
    ) {
        this.debug.log("app", "constructor");
        this.query.setConnection(this.db, this.storage);
        if (!this.existsDB()) {
            this.debug.log("app", "db not exists");
            if (this.existsDBInStorage()) {
                this.debug.log("app", "db in storage");
                if (this.isAttachable()) {
                    this.debug.log("app", "db attachable");
                    this.attach();
                }
                this.use();
            } else {
                this.createDB();
            }
        }
        this.config.init();
        this.settings.init();
        this.applySettings();
    }

    applySettings(): void {
        let bg_img = this.settings.get("background image");
        this.debug.log("app apply user settings");
        if(bg_img === "none" || bg_img === "") { return; }
        this.debug.log("app apply add bg", bg_img);
        this.bg_style = {
            position: "absolute",
            width: "100%",
            height: "100%",
            "z-index": "-100",
            "background-image": "url(" + bg_img + ")",
            "background-repeat": "no-repeat"
        };
    }

    existsDB(): boolean {
        let dbs = this.query.showDatabases().exec();
        let found: boolean = false;
        this.debug.log("app db check dbs", dbs);
        dbs.forEach((db: any) => {
            if (db.databaseid === this.db) {
                found = true;
            }
        });
        this.debug.log("app db check found", found);
        return found;
    }

    existsDBInStorage(): boolean {
        return localStorage.getItem(this.db) !== null;
    }

    isAttachable(): boolean {
        return true;
    }

    use(): boolean {
        let result = this.query.use().exec();
        this.debug.log("app db use result", result);
        return result;
    }

    attach(): boolean {
        let result = this.query.attach().exec();
        this.debug.log("app db attach result", result);
        return result;
    }

    createDB(): boolean {
        let result = this.query.createDB().exec();
        this.debug.log("app db crtDB result", result);
        return result;
    }

}
