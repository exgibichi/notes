import { Component, HostListener, Input } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

import { CategoriesService } from "../services/categories.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";
import { SettingsService } from "../services/settings.service";

export class ItemsComponent {
    private NONE_TYPE: number = -1;
    private FIRST_TYPE: number = 0;
    private SECOND_TYPE: number = 1;
    private BOTH_TYPE: number = 2;
    orders = { create_date: "Create date", update_date: "Update date", prior: "Prior" };
    order_by: string = "create_date";
    order_type: string = "ASC";
    add_or_edit: boolean = false;
    types = [];
    title;
    keys_buffer = [];
    item;
    items;
    second_items;
    selected_category;
    item_name: string;
    parse_urls: number = 1;
    show_second: number = 0;
    show_second_category_id: number = 0;
    transfer_modal: boolean = false;
    transfer_cat_id: number;
    transfer_item_id: number;
    cats;

    constructor(
        protected categories_service: CategoriesService,
        protected items_service, // : ListService | TodoService | NoteService,
        protected route: ActivatedRoute,
        protected debug: DebugService,
        protected alert: NotificationsService,
        protected settings: SettingsService,
    ) {
    }

    init() {
        this.route.params.forEach((params: Params) => {
            let id = +params["category_id"];
            this.debug.log("items cat_id: ", id);
            if (!id) { return; }
            this.selected_category = this.categories_service.getById(id);
            this.debug.log("items selected cat ", this.selected_category);
            this.debug.log("type id ", this.FIRST_TYPE);
            if (!this.selected_category) { return; }
            this.items = this.getOrderedItems(this.types.length !== 0 ? this.FIRST_TYPE : this.NONE_TYPE);
            this.show_second = 0;
        });
    }

    extractUrls(text): Array<string> { // TODO: transfer to utils
        if (this.parse_urls === 0) {
            return [];
        }
        let exp = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@!:%_\+.~#?&//=]*)/gi;
        let regex = new RegExp(exp);
        let urls = text.match(regex);
        if (urls) {
            return urls;
        }
        return [];
    }

    @HostListener('document:keyup', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        let cancel_combination = ["control", "shift", "z"];
        if (!this.add_or_edit) {
            if (event.key === "A") {
                this.showAdd();
            }
        } else {
            this.keys_buffer.push(event.key);
            if (this.keys_buffer.length > 3) {
                this.keys_buffer.shift();
            }
            if (this.keys_buffer.length === 3) {
                let tmp_buffer = this.keys_buffer.slice();
                let same = 0;
                tmp_buffer.forEach((key, index) => {
                    if (cancel_combination[index] === key.toLowerCase()) {
                        same++;
                    }
                });
                if (same === 3) {
                    this.cancelItemEdit();
                }
            }
        }
    }

    toggleParse() {
        this.parse_urls = this.parse_urls ? 0 : 1;
    }

    showAdd(): void {
        this.add_or_edit = true;
    }

    addItem(title: string, text?: string): void {
        let urls;
        title = title.trim();
        if (!title) { return; }
        if (text) {
            urls = this.extractUrls(text);
            if (this.items_service.create(title, text, this.selected_category.id, urls)) {
                this.alert.success("Success", this.item_name + " added succefuly");
                this.items = this.getOrderedItems(this.NONE_TYPE);
                this.updateCategoryCount();
                this.add_or_edit = false;
            } else {
                this.alert.error("Error", "Cant add " + this.item_name);
            }
        } else {
            urls = this.extractUrls(title);
            if (this.items_service.create(title, this.selected_category.id, urls)) {
                this.alert.success("Success", this.item_name + " added succefuly");
                this.items = this.getOrderedItems(this.FIRST_TYPE);
                this.updateCategoryCount(this.types[this.FIRST_TYPE]);
                this.add_or_edit = false;
            } else {
                this.alert.error("Error", "Cant add " + this.item_name);
            }
        }
    }

    editItem(item_id: number): void {
        if (!item_id) { return; }
        this.item = this.items_service.getById(item_id);
        this.add_or_edit = true;
    }

    cancelItemEdit(): void {
        this.item = undefined;
        this.add_or_edit = false;
    }

    saveItem(item: any, new_title: string, new_text?: string): void {
        let urls;
        if (!item) { return; }
        if (new_text) {
            urls = this.extractUrls(new_text);
            if (this.items_service.save(item.id, new_title, new_text, urls)) {
                this.alert.success("Success", this.item_name + " saved succefuly");
                this.items = this.getOrderedItems(this.NONE_TYPE);
                this.item = undefined;
                this.add_or_edit = false;
            } else {
                this.alert.error("Error", "Cant save " + this.item_name);
            }
        } else {
            urls = this.extractUrls(new_title);
            this.debug.log("edit urls", urls);
            if (this.items_service.save(item.id, new_title, urls)) {
                this.alert.success("Success", this.item_name + " saved succefuly");
                this.items = this.getOrderedItems(this.FIRST_TYPE);
                this.item = undefined;
                this.add_or_edit = false;
            } else {
                this.alert.error("Error", "Cant save " + this.item_name);
            }
        }
    }

    changeType(item_id: number, type: string, ok_msg, error_msg): void {
        if (!item_id) { return; }
        if (this.items_service.changeType(item_id, type)) {
            this.alert.success("Success", this.item_name + " " + ok_msg + " succefuly");
            if (this.item !== undefined && this.item.id === item_id) {
                this.item = undefined;
                this.add_or_edit = false;
            }
            this.updateCategoryCount(this.types[this.BOTH_TYPE]);
        } else {
            this.alert.error("Error", "Cant " + error_msg + " " + this.item_name);
        }
    }

    removeItem(item_id: number, isSecond?: number): void {
        if (!confirm("Delete this " + this.item_name + "?")) { return; }
        if (!item_id) { return; }
        if (this.items_service.remove(item_id)) {
            this.alert.success("Success", this.item_name + " removed succefuly");
            if (this.item !== undefined && this.item.id === item_id) {
                this.item = undefined;
                this.add_or_edit = false;
            }
            this.updateCategoryCount(this.types[isSecond ? this.SECOND_TYPE : this.FIRST_TYPE]);
        } else {
            this.alert.error("Error", "Cant remove " + this.item_name);
        }
    }

    showSecondItems() {
        if (this.show_second) {
            this.show_second = this.show_second ? 0 : 1;
        } else {
            if (!this.second_items.length || this.show_second_category_id !== this.selected_category.id) {
                this.show_second_category_id = this.selected_category.id;
                this.second_items = this.getOrderedItems(this.SECOND_TYPE);
            }
            this.show_second = this.show_second ? 0 : 1;
        }
    }

    updateCategoryCount(type?: string): void {
        let cnt = this.selected_category.cnt;
        let acnt = this.selected_category.acnt;
        if (type) {
            if (type === this.types[this.FIRST_TYPE] || type === this.types[this.BOTH_TYPE]) {
                this.items = this.getOrderedItems(this.FIRST_TYPE);
                cnt = this.items.length;
            }
            if (type === this.types[this.SECOND_TYPE] || type === this.types[this.BOTH_TYPE]) {
                this.second_items = this.getOrderedItems(this.SECOND_TYPE);
                acnt = this.second_items.length;
            }
        } else {
            this.items = this.getOrderedItems(this.NONE_TYPE);
            cnt = this.items.length;
        }
        this.categories_service.update(+this.selected_category.id, { cnt: cnt, acnt: acnt });
    }

    changePriorItem(item_id, up): void {
        if (!item_id) { return; }
        if (this.items_service.changePrior(item_id, up)) {
            this.items = this.getOrderedItems(this.FIRST_TYPE);
            this.alert.success("Success", "Prior changed succefully");
        } else {
            this.alert.error("Error", "Cant change prior");
        }
    }

    orderChange(value): void {
        this.debug.log("new order value", value);
        this.order_by = value;
    }

    reorder(type?: string): void { // todo optimize dont get by sql sort in front or sort in service
        this.changeOrder();
        if (type && this.types.length !== 0) {
            if (type === this.types[this.FIRST_TYPE]) {
                this.items = this.getOrderedItems(this.FIRST_TYPE, true);
            } else {
                this.second_items = this.getOrderedItems(this.SECOND_TYPE, true);
            }
        } else {
            this.items = this.getOrderedItems(this.NONE_TYPE, true);
        }
    }

    getOrderedItems(type: number, by_reorder?: boolean) {
        let setting_order_by = "saved_" + type + "_" + this.item_name + "_cat_id_" + this.selected_category.id + "_order_by";
        let setting_order_type = "saved_" + type + "_" + this.item_name + "_cat_id_" + this.selected_category.id + "_order_type";
        let saved_order_by = this.settings.get(setting_order_by);
        let saved_order_type = this.settings.get(setting_order_type);
        let order_by;
        let order_type;
        let items;
        if (by_reorder) {
            let same = saved_order_by === this.order_by && saved_order_type === this.order_type;
            if (same) {
                this.changeOrder();
            }
            order_by = this.order_by;
            order_type = this.order_type;
            this.settings.set(setting_order_by, order_by);
            this.settings.set(setting_order_type, order_type);
        } else {
            if (saved_order_by && saved_order_type) {
                this.order_by = order_by = saved_order_by;
                this.order_type = order_type = saved_order_type;
            } else {
                order_by = this.order_by;
                order_type = this.order_type;
            }
        }
        items = this.getItems(order_by, order_type, type !== -1 ? this.types[type] : false);
        items = this.replaceUrls(items);
        items = this.replaceEOL(items);
        return items;
    }

    getItems(order_by, order_type, type?) {
        if (type) {
            return this.items_service.getItemsByCategoryId(
                this.selected_category.id,
                type,
                order_by,
                order_type
            );
        } else {
            return this.items_service.getItemsByCategoryId(
                this.selected_category.id,
                order_by,
                order_type
            );
        }
    }

    replaceEOL(items) {
        items.forEach((value, key) => {
            items[key].text = items[key].text.split("\n").join("<br/>");
        });
        return items;
    }

    replaceUrls(items) {
        let tmp = [];
        items.forEach((value, key) => {
            let is_valid_urls = value.urls !== undefined && Array.isArray(value.urls) && value.urls.length !== 0;
            if (is_valid_urls) {
                let urls = value.urls;
                urls = urls.sort((a, b) => b.length - a.length);
                tmp = [];
                urls.forEach((url, url_key) => {
                    let tr = [url_key + "}}}{{{}}}", url];
                    tmp.push(tr);
                    items[key].text = items[key].text.
                        replace(url, "<a href=\"" + tr[0] + "\" target=\"_blank\">" + tr[0] + "</a>");
                });
                tmp.forEach((url) => {
                    let regex = new RegExp(url[0], "g");
                    items[key].text = items[key].text.replace(regex, url[1]);
                });
            }
        });
        return items;
    }

    chooseCategory(cat_id) {
        this.transfer_cat_id = cat_id;
    }

    cancelTransfer() {
        this.transfer_modal = false;
        this.cats = null;
        this.transfer_cat_id = 0;
        this.transfer_item_id = 0;
    }

    transfer(item_id: number, cat_id?: number) {
        if (!cat_id) {
            this.transfer_modal = true;
            this.transfer_item_id = item_id;
            this.cats = this.categories_service.getCategories(this.title.toLowerCase());
        } else {
            if (this.items_service.transfer(this.transfer_item_id, this.transfer_cat_id)) {
                this.alert.success("Success", this.item_name + " transfered succefuly");
                this.items = this.getOrderedItems(this.FIRST_TYPE);
                this.categories_service.update(+this.selected_category.id, { cnt: this.selected_category.cnt - 1 });
                let transfer_cat = this.categories_service.getById(this.transfer_cat_id);
                this.categories_service.update(+transfer_cat.id, { cnt: transfer_cat.cnt + 1 });
                this.cancelTransfer();
            }
        }
    }

    changeOrder(): void {
        if (this.order_type === "ASC") {
            this.order_type = "DESC";
        } else {
            this.order_type = "ASC";
        }
    }
}
