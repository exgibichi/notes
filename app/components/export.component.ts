import { Component } from "@angular/core";
import { Http } from "@angular/http";

import { Dropbox } from "../libs/dropbox";

import { AuthService } from "../services/auth.service";
import { ConfigService } from "../services/config.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";

declare let saveAs: any;
declare let base64: any;
declare let CryptoJS: any;

@Component({
    moduleId: module.id,
    templateUrl: "../views/export.component.html"
})

export class ExportComponent {
    title = "Export data";

    constructor(
        private dropbox: Dropbox,
        private auth_service: AuthService,
        private http: Http,
        private config: ConfigService,
        private debug: DebugService,
        private alert: NotificationsService
    ) {
        let dropbox_app_key = this.config.get("dropbox_app_key");
        let model = this.auth_service.get("dbox");

        if (model) {
            this.dropbox.init(this.debug, this.http, dropbox_app_key, model.token);
        }
    }

    exportToDbox(): void {
        let export_data = this.extractDataFromDb();
        let blob = new Blob([JSON.stringify(export_data)], {type: "text/plain;charset=utf-8"});
        let time = +new Date();
        this.dropbox.upload("export_" + time + ".txt", blob).then(result => {
            this.debug.log("up_result_exp", result);
            if (result) {
                this.alert.success("Success", "DB exported succefuly");
            } else {
                this.alert.error("Error", "Cant export db");
            }
        });
    }

    exportData(): void {
        let export_data = this.extractDataFromDb();
        let blob = new Blob([JSON.stringify(export_data)], {type: "text/plain;charset=utf-8"});
        let time = +new Date();
        if (saveAs(blob, "notes_export_" + time + ".txt")) {
            this.alert.success("Success", "DB exported succefuly");
        } else {
            this.alert.error("Error", "Cant export db");
        }
    }

    extractDataFromDb() {
        let storage_fields = [
            "alasql",
            "notes",
            "notes.categories",
            "notes.lists",
            "notes.settings",
            "notes.notes",
            "notes.todo"
        ];
        let data = {};
        let export_data = {};
        let version = this.config.get("version");
        storage_fields.forEach(field => {
            let field_data = localStorage.getItem(field);
            if (field_data !== null) {
                data[field] = base64.encode(field_data);
                data[field + "_hash"] = CryptoJS.MD5(field_data).toString();
            }
        });
        export_data["data"] = base64.encode(JSON.stringify(data));
        export_data["hash"] = CryptoJS.MD5(JSON.stringify(data)).toString();
        export_data["app_version"] = version;
        export_data["db_version"] = 1;
        return export_data;
    }

}
