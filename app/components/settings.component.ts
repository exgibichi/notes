import { Component } from "@angular/core";

import { SettingsService } from "../services/settings.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";

@Component({
    moduleId: module.id,
    templateUrl: "../views/settings.component.html"
})

export class SettingsComponent {
    title = "User settings";
    values;

    constructor(
        private debug: DebugService,
        private settings: SettingsService,
        private alert: NotificationsService
    ) {
        this.values = this.settings.getAll();
        this.debug.log("settings values", this.values);
    }

    truncate(): void {
        this.settings.truncate();
    }

    save(key, value) {
        if (this.settings.set(key, value)) {
            this.alert.success("Success", "Settings saved succefuly");
            this.values = this.settings.getAll();
        } else {
            this.alert.error("Error", "Cant save settings");
        }
    }
}
