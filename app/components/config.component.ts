import { Component } from "@angular/core";

import { ConfigService } from "../services/config.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";

@Component({
    moduleId: module.id,
    templateUrl: "../views/config.component.html"
})

export class ConfigComponent {
    title = "App config";
    values;

    constructor(
        private debug: DebugService,
        private config: ConfigService,
        private alert: NotificationsService
    ) {
        this.values = this.config.getAll();
        this.debug.log("config values", this.values);
    }

    save(key, value) {
        if (this.config.set(key, value)) {
            this.alert.success("Success", "Config saved succefuly");
            this.values = this.config.getAll();
        } else {
            this.alert.error("Error", "Cant save config");
        }
    }
}
