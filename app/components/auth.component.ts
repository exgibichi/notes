import { Component, OnInit } from "@angular/core";

import { Dropbox } from "../libs/dropbox";
import { Http } from "@angular/http";
import { Location } from "@angular/common";

import { AuthService } from "../services/auth.service";
import { ConfigService } from "../services/config.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";

@Component({
    moduleId: module.id,
    selector: "auth",
    templateUrl: "../views/auth.component.html"
})
export class AuthComponent implements OnInit {
    title = "Auth in cloud storages";
    is_logged = {"dbox": 0};
    current_url = window.location.origin + window.location.pathname;
    private dropbox_app_key;

    constructor(
        private dropbox: Dropbox,
        private http: Http,
        private url: Location,
        private auth_service: AuthService,
        private config: ConfigService,
        private debug: DebugService,
        private alert: NotificationsService
    ) {
        let model = this.auth_service.get("dbox");
        this.dropbox_app_key = this.config.get("dropbox_app_key");
        if (model) {
            this.dropbox.init(this.debug, this.http, this.dropbox_app_key, model.token);
        }
        this.checkAuth();
    }

    ngOnInit(): void {
        let params = this.extractParams();
        this.debug.log("params: ", params);
        if (params && params["state"] !== undefined) {
            switch (params["state"]) {
                case "dbox":
                if (params["access_token"] !== undefined) {
                    if (this.auth_service.checkToken("dbox")) {
                        this.auth_service.update(
                            "dbox",
                            params["access_token"],
                            decodeURIComponent(params["account_id"])
                        );
                    } else {
                        this.auth_service.addToken(
                            "dbox",
                            params["access_token"],
                            decodeURIComponent(params["account_id"])
                        );
                    }
                    this.checkAuth();
                }
                break;
            }
        }
    }

    checkAuth(): void {
        let model = this.auth_service.get("dbox");
        if (model && (model.expired === 0 || model.expired === "0")) {
            this.dropbox.checkAuth(model.uid).then(
                (dbox_result) => {
                    this.debug.log("dbox_result", dbox_result);
                    if (dbox_result) {
                        this.alert.success("Success", "You are logged to dropbox");
                        this.debug.log("logged");
                        this.is_logged.dbox = 1;
                    } else {
                        this.alert.alert("Warning", "Auth data expired");
                        this.debug.log("expired");
                        this.auth_service.setExpired("dbox");
                        this.is_logged.dbox = 0;
                    }
                }
            );
        } else {
            this.is_logged.dbox = 0;
            this.debug.log("not logged or expired");
        }
    }

    loginDbox(): void {
        this.debug.log("dbox login");
        window.location.href = this.dropbox.compileAuthUrl(this.dropbox_app_key, this.current_url);
    }

    logoutDbox(): void {
        this.debug.log("dbox logout");
        if (this.auth_service.setExpired("dbox")) {
            this.alert.success("Success", "Logout succefuly");
            this.checkAuth();
        } else {
            this.alert.error("Error", "Cant logout");
        }
    }

    extractParams(): string[] {
        let re = /[?&#]([^=#&]+)=([^&#]*)/g;
        let match;
        let isMatch = true;
        let matches = [];
        while (isMatch) {
            match = re.exec(window.location.href);
            if (match !== null) {
                matches[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
                if (match.index === re.lastIndex) {
                    re.lastIndex++;
                }
            }
            else {
                isMatch = false;
            }
        }
        return matches;
    }
}
