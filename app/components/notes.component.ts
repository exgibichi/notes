import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { NoteEntry } from "../types/note-entry";

import { CategoriesService } from "../services/categories.service";
import { NoteService } from "../services/note.service";
import { DebugService } from "../services/debug.service";
import { NotificationsService } from "angular2-notifications";
import { SettingsService } from "../services/settings.service";

import { ItemsComponent } from "./items.component";

@Component({
    moduleId: module.id,
    templateUrl: "../views/notes.component.html"
})

export class NotesComponent extends ItemsComponent implements OnInit {
    title = "Notes";
    item: NoteEntry;
    items: NoteEntry[] = [];
    item_name = "note";

    constructor(
        protected categories_service: CategoriesService,
        protected note_service: NoteService,
        protected route: ActivatedRoute,
        protected debug: DebugService,
        protected alert: NotificationsService,
        protected settings: SettingsService,
    ) {
        super(categories_service, note_service, route, debug, alert, settings);
    }

    ngOnInit(): void {
        super.init();
    }
}
